// Maximize all windows on startup
browser.windows.getAll()
  .then(windows => windows.map(win => browser.windows.update(win.id, { state: 'maximized' })))
  .then(promises => Promise.allSettled(promises))
  .then(results => results.forEach(result => {
    if (result.status === 'rejected') console.error(result.reason)
  }))
