// Clear all cookies in default container on startup
browser.cookies.getAll({
  firstPartyDomain: null,
  storeId: 'firefox-default'
})
  .then(cookies => cookies.map(cookie => browser.cookies.remove({
    name: cookie.name,
    firstPartyDomain: cookie.firstPartyDomain,
    url: `https://${cookie.domain}${cookie.path}`,
    storeId: 'firefox-default'
  })))
  .then(promises => Promise.allSettled(promises))
  .then(results => results.forEach(result => {
    if (result.status === 'rejected') console.error(result.reason)
  }))
