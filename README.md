# Bits and Bobs by WJH

A simple browser extension with miscellaneous quality of life improvements.

## What does it do?

Two things, currently!

1. **Maximizes all windows on startup.** Firefox's [fingerprinting protection](https://support.mozilla.org/en-US/kb/firefox-protection-against-fingerprinting) creates new windows at a standard size, but I prefer using a maximized window.
2. **Deletes all cookies in the default container on startup.** I use containers for all sites that I regularly use, and I prefer a clean slate for everything else.
